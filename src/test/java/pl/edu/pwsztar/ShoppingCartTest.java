package pl.edu.pwsztar;

import java.util.List;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static pl.edu.pwsztar.ShoppingCartOperation.PRODUCTS_LIMIT;

public class ShoppingCartTest {

  static ShoppingCartOperation shoppingCart;

  @BeforeEach
  void setUp() {
    shoppingCart = new ShoppingCart();
  }

  @Test
  void shouldAddUniqueProduct() {
    boolean result = shoppingCart.addProducts("tv", 100, 1);

    assertTrue(result);
  }

  @Test
  void shouldDeleteProductIfExists() {
    shoppingCart.addProducts("tv", 100, 1);

    boolean result = shoppingCart.deleteProducts("tv", 1);

    assertTrue(result);
  }

}

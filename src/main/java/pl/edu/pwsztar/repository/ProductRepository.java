package pl.edu.pwsztar.repository;

import java.util.Collection;
import java.util.Optional;
import pl.edu.pwsztar.entity.Product;

public interface ProductRepository {

  void upsert(Product product);

  boolean deleteByName(String name);

  Optional<Product> getProductByName(String name);

  Collection<Product> getAllProducts();
}

package pl.edu.pwsztar;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import pl.edu.pwsztar.entity.Product;
import pl.edu.pwsztar.repository.ProductRepository;
import pl.edu.pwsztar.repository.impl.ProductRepositoryImpl;

public class ShoppingCart implements ShoppingCartOperation {

  final ProductRepository repository;

  public ShoppingCart() {
    repository = new ProductRepositoryImpl();
  }

  public boolean addProducts(String productName, int price, int quantity) {
    if (price <= 0
        || quantity <= 0
        || getQuantityOfProducts() >= ShoppingCartOperation.PRODUCTS_LIMIT) {
      return false;
    }
    Optional<Product> foundProductOptional = repository.getProductByName(productName);
    if (foundProductOptional.isPresent()) {
      Product foundProduct = foundProductOptional.get();
      if (foundProduct.getPrice() == price) {
        Product productToSave =
            new Product(productName, price, quantity + foundProduct.getQuantity());
        repository.upsert(productToSave);
        return true;
      }
      return false;
    } else {
      Product productToSave = new Product(productName, price, quantity);
      repository.upsert(productToSave);
      return true;
    }
  }

  public boolean deleteProducts(String productName, int quantity) {
    if (quantity <= 0) {
      return false;
    }
    int quantityOfProduct = getQuantityOfProduct(productName);
    if (quantityOfProduct == 0) {
      return false;
    }
    if (quantityOfProduct < quantity) {
      return false;
    } else if (quantityOfProduct > quantity) {
      Product product = repository.getProductByName(productName).get();
      Product updatedProduct =
          new Product(product.getName(), product.getPrice(), product.getQuantity() - quantity);
      repository.upsert(updatedProduct);
      return true;
    } else {
      return repository.deleteByName(productName);
    }
  }

  public int getQuantityOfProduct(String productName) {
    return repository.getProductByName(productName)
        .map(Product::getQuantity)
        .orElse(0);
  }

  public int getSumProductsPrices() {
    return repository.getAllProducts()
        .stream()
        .mapToInt(p -> p.getPrice() * p.getQuantity())
        .sum();
  }

  public int getProductPrice(String productName) {
    return repository.getProductByName(productName)
        .map(Product::getPrice)
        .orElse(0);
  }

  public List<String> getProductsNames() {
    return repository.getAllProducts()
        .stream()
        .map(Product::getName)
        .collect(Collectors.toList());
  }

  private int getQuantityOfProducts() {
    return repository.getAllProducts()
        .stream()
        .mapToInt(Product::getQuantity)
        .sum();
  }
}
